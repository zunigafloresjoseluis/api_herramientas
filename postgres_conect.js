const Sequelize=require('sequelize'),
    datos_connc=require('./datos_base')

const sequelize=new Sequelize(datos_connc.postgres.db,datos_connc.postgres.user,datos_connc.postgres.password,{
    dialect:'postgres',
    port:datos_connc.postgres.port,
    pool:{
        max:5,
        min:0,
        idle:10000
      
    }
});
sequelize
    .authenticate()
    .then(()=>{console.log(`Conexión realizada`)})
    .catch(err=>{console.log(`Conexión fallida :'( ${err.stack}`)})

module.exports=sequelize;


