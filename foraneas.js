var Actividad = require('./modelos/Actividad'),
    Correos = require('./modelos/Correos'),
    Empresa = require('./modelos/Empresa'),
    Entidad = require('./modelos/Entidad'),
    Referencias = require('./modelos/Referencias'),
    Ubicacion = require('./modelos/Ubicacion'),
    UbicacionEspecifica = require('./modelos/UbicacionEspecifica'),
    Telefonos = require('./modelos/Telefonos')

Ubicacion.belongsTo(Empresa, {
    foreignKey: 'id_empresa',
    sourceKey: 'id'
})

Empresa.hasMany(Ubicacion, {
    foreignKey: 'id_empresa',
    sourceKey: 'id'
})

Ubicacion.belongsTo(Entidad, {
    foreignKey: 'clave_entidad',
    sourceKey: 'clave'
})
Correos.belongsTo(Empresa, {
    foreignKey: 'id_empresa',
    sourceKey: 'id'
})

Empresa.hasMany(Correos, {
    foreignKey: 'id_empresa',
    sourceKey: 'id'
})

Telefonos.belongsTo(Empresa, {
    foreignKey: 'id_empresa',
    sourceKey: 'id'
})

Empresa.hasMany(Telefonos, {
    foreignKey: 'id_empresa',
    sourceKey: 'id'
})

Ubicacion.belongsTo(UbicacionEspecifica, {
    foreignKey: 'id_ubicacion',
    sourceKey: 'id_ubicacion'
})
UbicacionEspecifica.belongsTo(Ubicacion, {
    foreignKey: 'id_ubicacion',
    sourceKey: 'id_ubicacion'
})

UbicacionEspecifica.hasMany(Referencias, {
    foreignKey: 'id_especifica',
    sourceKey: 'id_especifica'
})

Referencias.belongsTo(UbicacionEspecifica, {
    foreignKey: 'id_especifica',
    sourceKey: 'id_especifica'
})

Actividad.hasMany(Empresa, {
    foreignKey: 'idactividad',
    sourceKey: 'cve_scian'

})

Empresa.belongsTo(Actividad, {
    foreignKey: 'idactividad',
    sourceKey: 'cve_scian'
})