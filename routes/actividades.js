const Router = require('restify-router').Router,
    router = new Router(),
    parser = require('js2xmlparser2'),
    js2xmlparser = require('js2xmlparser2'),
    Actividad = require('../modelos/Actividad'),
    sequealize = require('../postgres_conect'),
    Entidad = require('../modelos/Entidad'),
    validacion = require('../funciones_validaciones/validar'),
    Correo = require('../modelos/Correos'),
    cb = new validacion();

router.get('/', (req, res, next) => {
    let respuesta = { actividad: [] };

    let limites=cb.limites(req.query.limite_inferior,req.query.limite_superior);
    let orden=cb.orden(req.query.tipo_orden);
    let busqueda='%';
    if(req.query.nombre_actividad!=undefined){
        busqueda='%'+req.query.nombre_actividad+'%';
    }
    if(limites[0]===-1||orden==="error"){
        res.status(404);
        res.end();
        next();
    }else{
    if(orden==="id"){
        orden='cve_scian';
    }else{
        orden='desc_scian';
    }

    Actividad.findAll({
        order:[orden],
        where: {desc_scian :{$ilike: busqueda}}
    })
        .then(data => {
            if (data.length > 0) {
                if(limites[1]>data.length){
                    limites[1]=data.length;
                }
                for (let i = limites[0]; i < limites[1]; i++) {
                    respuesta.actividad.push({
                        clave_actividad: data[i].dataValues.cve_scian,
                        descripcion_actividad: data[i].dataValues.desc_scian,
                    })
                }
                switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);
                        res.end(parser("actividades", respuesta, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;
                }
            } else {
                res.status(404);
                res.end();
                next();
            }
        })
        .catch(err => {
            console.log(err.stack)
        })

    }

})


router.get('/estados/:id', (req, res, next) => {
    let busqueda='%';
    if(req.query.nombre_actividad!=undefined){
        busqueda='%'+req.query.nombre_actividad+'%';
    }

    var respuesta={estado:[]};
    sequealize.query(`select distinct cve_scian,desc_scian,clave_entidad,entidad.nombre from actividad 
        inner join  empresa on (idactividad=cve_scian)
        inner join  ubicacion on(id_empresa=id)
        inner join  entidad  on (clave_entidad=clave) where clave_entidad='${req.params.id}' 
        and desc_scian ilike '${busqueda}'
        `, {
            type: sequealize.QueryTypes.SELECT
        })
        .then(datos => {
            // console.log(datos);
            if(datos.length>0){

            for (let q = 0; q < datos.length; q++) {
                if (!cb.existe_estado(respuesta.estado, datos[q].clave_entidad)) {
                    respuesta.estado.push({ id: datos[q].clave_entidad, nombre: datos[q].nombre, actividades:[] })
                }
            }
            
            for (let i = 0; i < datos.length; i++) {
                for (let j = 0; j < respuesta.estado.length; j++) {
                    if (respuesta.estado[j].id === datos[i].clave_entidad) {
                        let obj_entregar = {};
                        obj_entregar.clave_actividad=datos[i].cve_scian;
                        obj_entregar.descripcion_actividad=datos[i].desc_scian;
                        respuesta.estado[j].actividades.push(obj_entregar);
                    }
                }
            }
            
            switch (cb.cabeceras(req.headers.accept)) {
                    case 'application/xml':
                        res.setHeader('Content-Type', 'application/xml');
                        res.status(200);
                        res.end(parser("estados", respuesta, { useCDATA: false }));
                        break;
                    case 'application/json':
                        res.setHeader('Content-Type', 'application/json');
                        res.json(200, respuesta);
                        break;
                    default:
                        res.status(406);
                        res.end();
                        break;
                }
            console.log(obj_entregar);
        }else {
                res.status(404);
                res.end();
                next();
            }
        })
        .catch(err => {
            console.log(err.stack)
        })
        
})


router.post('/', (req, res, next) => {
    console.log(req.body);
    return sequealize.transaction(trans => {
            return Actividad.create({
                cve_scian: req.body.clave_actividad,
                desc_scian: req.body.descripcion_actividad
            }, { transaction: trans });
        })
        .then(data => {
            res.status(201);
            res.end();
            next();
        })
        .catch(err => {
            console.log(err.stack)
            res.status(400);
            res.end();
            next();
        })
})

router.put('/', (req, res, next) => {
    return sequealize.transaction(trans => {
            return Actividad.update({ desc_scian: req.body.descripcion_actividad }, {
                    where: {
                        cve_scian: req.body.clave_actividad
                    }
                }, {
                    returning: true,
                    plain: true,
                    transaction: trans
                }
            )
        })
        .then(resultado => {
            res.status(201);
            res.end();
            next();
        })
        .catch(err => {
            res.status(400);
            res.end();
            next();
        });
})

module.exports = router;