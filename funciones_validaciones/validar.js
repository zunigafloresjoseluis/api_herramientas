const xmlparser = require('js2xmlparser');

class validacion {
    cabeceras(arr) {
        var campos = arr.split(', ');
        var temporal;
        for (let i = 0; i < campos.length; i++) {
            if (campos[i] === '*/*') {
                temporal = '*/*';
                break;
            }
            if (campos[i] === 'application/xml') {
                temporal = 'application/xml';
                break;
            }
            if (campos[i] === 'application/json') {
                temporal = 'application/json';
                break;
            }
            if ((i + 1) === campos.length) {
                temporal = 'invalid';
                break;
            }
        };
        return temporal;


    }
    valida_nombre(nombre) {
        if (nombre) {
            return nombre
        } else {
            if (nombre == '')
                return null;
            else
                return '%%';
        }
    }

    existe_arr(arreglo, valor) {
        for (let i = 0; i < arreglo.length; i++) {
            if (arreglo[i] == valor) {
                return true;
            }
        }
        return false;
    }

    existe_estado(arreglo,valor){
        for (let i = 0; i < arreglo.length; i++) {
            if (arreglo[i].id == valor) {
                return true;
            }
        }
        return false;
    }

    limites(lim_inf,lim_sup){
        if(lim_inf===undefined&&lim_sup===undefined){
            return [0,100];
            console.log("no hay limites");
        }
        if(Number.isInteger(Number(lim_inf))){
            if(Number.isInteger(Number(lim_sup))){
                if(Number(lim_sup)>Number(lim_inf)&&lim_inf>=0&&(lim_sup-lim_inf)<=100){
                    return [Number(lim_inf),Number(lim_sup)];
                }else{
                    console.log("Los limites no estan bien definidos o son muy grandes");
                }

            }else{
                console.log("Error limite superior "+lim_sup);
            }

        }else{
            console.log("Error limite inferior "+lim_inf);
        }
        return [-1,-1];
    }

    orden(tipo_orden){
        console.log("ORDEN: "+tipo_orden);
        if(tipo_orden==="id"||tipo_orden===undefined){
            return "id";
        }else{
            if(tipo_orden==="nombre"){
                return "nombre";
        }else{
                return "error";
        }
        }
    }




}
module.exports = validacion;