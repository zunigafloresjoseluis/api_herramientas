var Sequelize=require('sequelize'),
    connection=require('../postgres_conect');

const Actividad=connection.define('actividad',{
    cve_scian:{
        type:Sequelize.INTEGER,
        field:"cve_scian", // es el nombre de la columna    
        primaryKey: true,
        allowNull:false,
        autoIncrement: true,
        validate:{
            not: ["[a-z]",'i'], 

        }  

    },
    desc_scian:{
        type:Sequelize.STRING,
        field:"desc_scian",
    }},{
        timestamps: false,
      freezeTableName: false,
      tableName: 'actividad'
  }
)

module.exports=Actividad;