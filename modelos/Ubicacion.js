var Sequelize = require('sequelize'),
    connection = require('../postgres_conect');

const Ubicacion = connection.define('ubicacion', {
    id_ubicacion: {
        type: Sequelize.NUMERIC,
        field: 'id_ubicacion',

        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
        validate: {
            not: ["[a-z]", 'i'],

        }
    },
    id_empresa: {
        type: Sequelize.BIGINT,
        allowNull: false,

        field: 'id_empresa'
    },


    clave_entidad: {
        type: Sequelize.STRING,
        field: 'clave_entidad'
    },

    clave_municipio: {
        type: Sequelize.STRING,
        field: 'clave_municipio'
    },
    nombre_municipio: {
        type: Sequelize.STRING,
        field: 'nombre_municipio'
    },
    clave_localidad: {
        type: Sequelize.STRING,
        field: 'clave_localidad'
    },
    nombre_localidad: {
        type: Sequelize.STRING,
        field: 'nombre_localidad'
    },
    cp: {
        type: Sequelize.STRING,
        field: 'cp'
    },
    agbasica: {
        type: Sequelize.STRING,
        field: 'agbasica'

    }


}, {
    timestamps: false,
    freezeTableName: false,
    tableName: 'ubicacion'
})

module.exports = Ubicacion;