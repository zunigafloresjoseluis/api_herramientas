var Sequelize = require('sequelize'),
    connection = require('../postgres_conect');

const Telefonos = connection.define('telefono', {
    id_empresa: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        field: 'id_empresa'
    },
    telefono: {
        type: Sequelize.STRING,
        allowNull: false

    }


}, {
    timestamps: false,
    freezeTableName: false,
    tableName: 'telefonos'
})


module.exports = Telefonos;